<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing data in a MySQL database using PDO.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */

require_once("AbstractModel.php");
require_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @todo implement class functionality.
 */
class DBModel extends AbstractModel
{
    protected $db = null;
    
    /**
     * @param PDO $db PDO object for the database; a new one will be created if no PDO object
     *                is passed
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
        } else {
            
                $this->db = new PDO('mysql:host=localhost;dbname=test;charset=utf8mb4', 'root', '');
                $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            



            // Create PDO connection
        
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookList()
    {
        $booklist = array();
        $query =  $this->db->prepare ( "SELECT * FROM book ORDER BY id" );
        $query->execute();
        $booklist = $query->fetchAll(PDO::FETCH_ASSOC);

        return $booklist;
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function getBookById($id)
    {
        $book = null;
        self::verifyId($id);
        $stmt = $this->db->prepare ("SELECT * FROM book WHERE id = :id");
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $book = $stmt->fetchAll();

        
        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param Book $book The book to be added - the id of the book will be set after successful insertion.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
     */
    public function addBook($book)
    {
        
            
        self::verifyBook($book);
        $eks = $this->db->prepare ("INSERT INTO Book
                                                 (title, author, description)
                                                 VALUE(:title,:author,:desc)");
        $eks->bindValue(":title", $book->title, PDO::PARAM_STR);
        $eks->bindValue(":author", $book->author, PDO::PARAM_STR);
        $eks->bindValue(":desc", $book->description, PDO::PARAM_STR);
        $eks->execute();
        $book->id = $this->db->lastInsertID();
        
    
}

    /** Modifies data related to a book in the collection
     * @param Book $book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function modifyBook($book)
    {
        self::verifyBook($book);

        $eks = $this->db->prepare('UPDATE Book
                                         SET title = :tit, author = :aut, description = :des
                                         WHERE id = :id');
              $eks->bindValue(':id', $book->id, PDO::PARAM_STR); //Binding attributes to chosen column
              $eks->bindValue(':tit', $book->title, PDO::PARAM_STR);
              $eks->bindValue(':aut', $book->author, PDO::PARAM_STR);
              $eks->bindValue(':des', $book->description, PDO::PARAM_STR);
              $eks->execute();

    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     * @todo Implement function using PDO and a real database.
     * @throws PDOException
    */
    public function deleteBook($id)
    {
        $stmt = $this->db->prepare ("DELETE FROM book WHERE :id");
        self::verifyId($id);
        $stmt->bindParam(":id", $book->id);
        $stmt->execute();
    

    }
}
