<?php

require_once('src/Model/DBModel.php');

class BookCollectionTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $dbModel;
    
    protected function _before()
    {
        $db = new PDO(
                'mysql:host=localhost;dbname=test;charset=utf8',
                'root',
                '',
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
            );
        $this->dbModel = new DBModel($db);
    }

    protected function _after()
    {
    }

    // Test that all books are retrieved from the database
    public function testGetBookList()
    {
        $bookList = $this->dbModel->getBookList();

        // Sample tests of book list contents
        $this->assertEquals(count($bookList), 3);
        $this->assertEquals(1, $bookList[0]["id"]);
        $this->assertEquals($bookList[0]["title"], 'Jungle Book');
        $this->assertEquals($bookList[1]["id"], 2);
        $this->assertEquals($bookList[1]["author"], 'J. Walker');
        $this->assertEquals($bookList[2]["id"], 3);
        $this->assertEquals($bookList[2]["description"], 'Written by some smart gal.');



    }

    // Tests that information about a single book is retrieved from the database
    public function testGetBook()
    {
        $book = $this->dbModel->getBookById(1);

        // Sample tests of book list contents
        $this->assertEquals($book[0]["id"], 1);
        $this->assertEquals($book[0]["title"], 'Jungle Book');
        $this->assertEquals($book[0]["author"], 'R. Kipling');
        $this->assertEquals($book[0]["description"], 'A classic book.');
    }

    // Tests that get book operation fails if id is not numeric
    public function testGetBookRejected()
    {
        try {
            $this->dbModel->getBookById("1'; drop table book;--");
            //$this->assertInstanceOf(InvalidArgumentException::class, null);
        } catch (InvalidArgumentException $e) {
        }
    }

    // Tests that a book can be successfully added and that the id was assigned. Four cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description" 
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function testAddBook()
    {
       /* $testValues1 = ['title' => 'New book',
                       'author' => 'Some author',
                       'description' => 'Some description'];

        $testValues2 = ['title' => 'New book',
                       'author' => 'Some author',
                       'description' => ''];

         $testValues3 = ['title' => "<script>document.body.style.visibility='hidden'</script>",
                       'author' => "<script>document.body.style.visibility='hidden'</script>",
                       'description' => "<script>document.body.style.visibility='hidden'</script>"];

        $testValuesLoop = [1 => $testValues1, 2 => $testValues2, 3 => $testValues3];
        $curretID = 4;
        for ($i = 1; $i <= 3; $i++){

            $testValues = $testValuesLoop[$i];
            $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
            $this->dbModel->addBook($book);

            
            
           
        }
        for ($i = 1; $i <= 3; $i++){

           // $this->assertEquals($book->id, $curretID);
            $this->tester->seeNumRecords(6, 'book');
            $this->tester->seeInDatabase('book', ['id' => 3+$i,
            'title' => $testValues['title'],
            'author' => $testValues['author'],
            'description' => $testValues['description']]);
            
            $curretID++;
            
        }*/


        $testValues1 = ['title' => 'New book',
        'author' => 'Some author',
        'description' => 'Some description'];
$testValues2 = ['title' => 'New book',
        'author' => 'Some author',
        'description' => ''];
$testValues3 = ['title'=>"<script>document.body.style.visibility='hidden'</script>",
'author'=>"<script>document.body.style.visibility='hidden'</script>",
'description'=>"<script>document.body.style.visibility='hidden'</script>"];
$loadTestValues = [1 => $testValues1, 2 => $testValues2, 3 => $testValues3 ];
//  first i create new books that will include all test cases
for ($i = 1; $i <= 3; $i++)
{
$testValues = $loadTestValues[$i];
$book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
$this->dbModel->addBook($book);
}
// and then check all of them for proper values
for ($i = 1; $i <= 3; $i++)
{
$testValues = $loadTestValues[$i];
// Id was successfully assigned
$this->assertEquals(3+$i, $book->id = 3+$i);
$this->tester->seeNumRecords(6, 'book');
// Record was successfully inserted
$this->tester->seeInDatabase('book', ['id' => 3+$i,
                                   'title' => $testValues['title'],
                                   'author' => $testValues['author'],
                                   'description' => $testValues['description']]);
}



    }

    // Tests that adding a book fails if id is not numeric
    public function testAddBookRejectedOnInvalidId()
    {
        $testValues = ['title' => 'New book', 'author' => 'Some author',
        'description' => '', 'id' => 'a'];
        $book = new Book($testValues['title'], $testValues['author'],
        $testValues['description'],$testValues['id']);

        try {
        $this->dbModel->addBook($book);
        $this->assertInstanceOf(InvalidArgumentException::class, null);
        } 
        catch (InvalidArgumentException $e) {
}

    }

    // Tests that adding a book fails mandatory fields are left blank
    public function testAddBookRejectedOnMandatoryFieldsMissing()
    {
        
    $testValues = ['title' => '',
                    'author' => '',
                    'description' => '',
                    'id' => ''];
     $book = new Book($testValues['title'], $testValues['author'],
     $testValues['description'],$testValues['id']);
     
    try {
    $this->dbModel->modifyBook($book);
    $this->assertInstanceOf(InvalidArgumentException::class, null);
     } 
    catch (InvalidArgumentException $e) {
}
        
    }

    // Tests that a book record can be successfully modified. Three cases should be verified:
    //   1. title=>"New book", author=>"Some author", description=>"Some description"
    //   2. title=>"New book", author=>"Some author", description=>""
    //   3. title=>"<script>document.body.style.visibility='hidden'</script>",
    //      author=>"<script>document.body.style.visibility='hidden'</script>",
    //      description=>"<script>document.body.style.visibility='hidden'</script>"
    public function testModifyBook()
    {
        /*$testValues = ['title' => 'New book', 'author' => 'Some author', 'description' => 'Some description'];
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description']);
        $this->dbModel->modifyBook($book); 




        $this->tester->seeInDatabase('book', [ 'title' => $testValues['title'], 'author' => $testValues['author'], 'description' => $testValues['description']]);
    */
         $testValues1 = ['title' => 'New book',
        'author' => 'Some author',
        'description' => 'Some description',
        'id' => 1];
        $testValues2 = ['title' => 'New book',
        'author' => 'Some author',
        'description' => '',
        'id' => 2];
        $testValues3 = ['title'=>"<script>document.body.style.visibility='hidden'</script>",
        'author'=>"<script>document.body.style.visibility='hidden'</script>",
        'description'=>"<script>document.body.style.visibility='hidden'</script>",
        'id' => 3];
        $loadTestValues = [1 => $testValues1, 2 => $testValues2, 3 => $testValues3 ];


        for ($i = 1; $i <= 3; $i++)
        {
            $testValues = $loadTestValues[$i];
            $book = new Book($testValues['title'], $testValues['author'], $testValues['description'], $testValues['id']);
            $this->dbModel->modifyBook($book);
            }
            for ($i = 1; $i <= 3; $i++)
{
$testValues = $loadTestValues[$i];
// Id was successfully assigned
//$this->assertEquals($i, $book->id);
$this->tester->seeNumRecords(3, 'book');
// Record was successfully inserted
$this->tester->seeInDatabase('book', ['id' => $i,
                                   'title' => $testValues['title'],
                                   'author' => $testValues['author'],
                                   'description' => $testValues['description']]);
}





    }
    
    // Tests that modifying a book record fails if id is not numeric
    public function testModifyBookRejectedOnInvalidId()
    {    
      /* $testValues = ['title' => 'New book', 'author' => 'Some author', 'description' => 'Some description','id' => 'Test'];
        $book = new Book($testValues['title'], $testValues['author'], $testValues['description'], $testValues['id']);
        $this->dbModel->modifyBook($book); */
        
        
        $this->tester->expectException(Exception::class, function() {
            $this->dbModel->modifyBook("1'; drop table book;--");
        });
    }
    
    // Tests that modifying a book record fails if mandatory fields are left blank
    public function testModifyBookRejectedOnMandatoryFieldsMissing()
    {  
        $testValues = ['title' => '',
                       'author' => '',
                       'description' => ''];
        $book = new Book($testValues['title'], $testValues['author'],
		$testValues['description']);
		
	try {
   $this->dbModel->modifyBook($book);
   
        } 
   catch (InvalidArgumentException $e) {
   }
        
    }
    
    // Tests that a book record can be successfully modified.
    public function testDeleteBook()
    {
        $id = 1;
	$this->dbModel->deleteBook($id);
	$this->tester->dontSeeInDatabase('book', ['id' => '$id']);
    }
    
    // Tests that adding a book fails if id is not numeric
    public function testDeleteBookRejectedOnInvalidId()
    {
        $id = 's';
        try{
        $this->dbModel->deleteBook($id);
        $this->assertInstanceOf(InvalidArgumentException::class, null);
            } 
       catch (InvalidArgumentException $e) {
       }
        }
}